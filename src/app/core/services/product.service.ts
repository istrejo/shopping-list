import { Injectable } from '@angular/core';
import { BehaviorSubject, Observable, Subject } from 'rxjs';
import { filter, map, reduce } from 'rxjs/operators';
import { Product } from '../models/product.model';

@Injectable({
  providedIn: 'root',
})
export class ProductService {
  private myProducts: Product[];
  private myProducts$ = new BehaviorSubject<Product[]>([]);

  constructor() {
    this.myProducts = [];
  }

  addProduct(product: Product) {
    this.myProducts.push(product);
    this.myProducts$.next(this.myProducts);
    console.log(this.myProducts);
  }

  getProducts$(): Observable<Product[]> {
    return this.myProducts$.asObservable();
  }

  deleteProduct(product: Product) {
    this.myProducts = this.myProducts.filter(
      (pro) => pro.title !== product.title
    );
    console.log(this.myProducts);
  }

  getTotal() {
    return this.myProducts
      .filter((pro) => !pro.completed)
      .map((pro) => pro.quantity * pro.price)
      .reduce((acu, pro) => acu + pro, 0);
  }
}
