export interface Product {
  title: string;
  price: number;
  quantity: number;
  completed: boolean;
}
