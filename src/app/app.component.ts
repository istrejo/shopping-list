import { Component } from '@angular/core';
import { Observable } from 'rxjs';
import { Product } from './core/models/product.model';
import { ProductService } from './core/services/product.service';

@Component({
  selector: 'app-root',
  templateUrl: './app.component.html',
  styleUrls: ['./app.component.scss'],
})
export class AppComponent {
  title = 'shopping-list';
}
