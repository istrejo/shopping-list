import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { ReactiveFormsModule } from '@angular/forms';

import { ProductListComponent } from './components/product-list/product-list.component';
import { ProductComponent } from './components/product/product.component';
import { TotalComponent } from './components/total/total.component';
import { ProductFormComponent } from './components/product-form/product-form.component';

@NgModule({
  declarations: [
    ProductListComponent,
    ProductComponent,
    TotalComponent,
    ProductFormComponent,
  ],
  imports: [CommonModule, ReactiveFormsModule],
  exports: [ProductListComponent, ProductComponent, ProductFormComponent],
})
export class ProductsModule {}
