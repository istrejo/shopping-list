import { Component, OnInit, Input, Output, EventEmitter } from '@angular/core';
import { ProductService } from 'src/app/core/services/product.service';

@Component({
  selector: 'app-total',
  templateUrl: './total.component.html',
  styleUrls: ['./total.component.scss'],
})
export class TotalComponent implements OnInit {
  @Input() message: string = '';
  @Input() total: any;

  // total: number = 0;

  constructor(private productService: ProductService) {}

  ngOnInit(): void {
    // this.productService.getProducts$().subscribe((products) => {
    //   this.total = products
    //     .filter((pro) => !pro.completed)
    //     .map((pro) => pro.quantity * pro.price)
    //     .reduce((acu, pro) => acu + pro, 0);
    // });
  }
}
