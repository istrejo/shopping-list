import { Component, OnInit, Input, Output, EventEmitter } from '@angular/core';
import { Product } from 'src/app/core/models/product.model';

@Component({
  selector: 'app-product',
  templateUrl: './product.component.html',
  styleUrls: ['./product.component.scss'],
})
export class ProductComponent implements OnInit {
  @Input() product: Product = {
    title: '',
    price: 0,
    quantity: 0,
    completed: false,
  };
  @Output() deleteProduct: EventEmitter<Product> = new EventEmitter();
  @Output() toggleProduct: EventEmitter<Product> = new EventEmitter();
  constructor() {}

  ngOnInit(): void {}

  onDelete(product: Product) {
    this.deleteProduct.emit(product);
  }

  onToggleCheck(product: Product) {
    product.completed = !product.completed;
    this.toggleProduct.emit(product);
  }
}
