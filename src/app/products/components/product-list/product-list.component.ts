import { Component, OnInit, OnChanges } from '@angular/core';
import { Observable } from 'rxjs';
import { filter, map, reduce } from 'rxjs/operators';
import { Product } from 'src/app/core/models/product.model';
import { ProductService } from 'src/app/core/services/product.service';

@Component({
  selector: 'app-product-list',
  templateUrl: './product-list.component.html',
  styleUrls: ['./product-list.component.scss'],
})
export class ProductListComponent implements OnInit {
  products: Product[] = [];
  total: number = 0;
  counter = 0;

  constructor(private productService: ProductService) {}

  ngOnInit(): void {
    this.productService.getProducts$().subscribe((products) => {
      this.products = products;
    });
    this.productService.getProducts$().subscribe((products) => {
      this.total = products
        .filter((pro) => !pro.completed)
        .map((pro) => pro.quantity * pro.price)
        .reduce((acu, pro) => acu + pro, 0);
    });
  }

  deleteProduct(product: Product) {
    this.productService.deleteProduct(product);
    this.products = this.products.filter((pro) => pro.title !== product.title);
    this.total = this.productService.getTotal();
  }

  toggleProduct(product: Product) {
    this.total = this.productService.getTotal();
  }
}
