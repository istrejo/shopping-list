import { Component, OnInit } from '@angular/core';

import { FormControl, FormGroup } from '@angular/forms';
import { ProductService } from 'src/app/core/services/product.service';

@Component({
  selector: 'app-product-form',
  templateUrl: './product-form.component.html',
  styleUrls: ['./product-form.component.scss'],
})
export class ProductFormComponent implements OnInit {
  form: FormGroup;

  constructor(private productService: ProductService) {
    this.form = new FormGroup({
      title: new FormControl(),
      price: new FormControl(),
      quantity: new FormControl(),
    });
  }

  ngOnInit(): void {}

  onSubmit() {
    this.productService.addProduct(this.form.value);
    this.form.reset();
  }
}
